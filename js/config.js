(function () {
    var controllerID       = "mainCTRL";
    var sliderControllerID = "slider";

    var app = angular.module("app");

    app.controller("mainCTRL", ['$scope', function ($scope) {
        var yes             = {display: "block"};
        var no              = {display: "none"};
        $scope.topSocial    = yes;
        $scope.topButtons   = no;
        $scope.homeBanner   = yes;
        $scope.Call1        = no;
        $scope.Call2        = no;
        $scope.leftSideBar  = no;
        $scope.rightSideBar = no;
        $scope.Footer1      = no;
        $scope.Footer2      = yes;
        $scope.credit       = yes;
    }]);

    app.factory('configService', function () {
        var store = {
            name: "Bay Motors",
            branch1: "Bay Motors",
            branch2: "",
            branch3: "",
            branch4: "",
            email1: "keith@baymotors.co.za",
            email2: "",
            email3: "",
            email4: "",
            facebookUrl: "https://www.facebook.com/baymotors",
            twitterUrl: "",
            googleplusUrl: "",
            youtubeUrl: "",
            address1: "Langeberg Park Opposite Langeberg Mall, Mossel Bay",
            address2: "",
            address3: "",
            address4: "",
            tel1: "082 831 2403",
            tel2: "",
            tel3: "",
            tel4: "",
            cell1: "082 831 2403",
            cell2: "082 831 2403",
            cell3: "",
            cell4: "",
            fax1: "044 695 1468",
            fax2: "",
            fax3: "",
            fax4: "",
            eMail: {
                contactEmail: "keith@baymotors.co.za",
                contactEmailTo: "Bay Motors",
                carRequestEmail: "keith@baymotors.co.za",
                carRequestEmailTo: "Bay Motors",
                carSellEmail: "keith@baymotors.co.za",
                carSellEmailTo: "Bay Motors",
                carServiceEmail: "keith@baymotors.co.za",
                carServiceEmailTo: "Bay Motors",
                carFinanceEmail: "keith@baymotors.co.za",
                carFinanceEmailTo: "Bay Motors Auto Finance",
                carEnquireEmail: "keith@baymotors.co.za",
                carEnquireEmailTo: "Bay Motors",
                testimonialEmail: "keith@baymotors.co.za",
                testimonialEmailTo: "Bay Motors",
                bccEmail: "webmaster@vmgsoftware.co.za",
                ccEmail: ""
            },
            tradinghours: {
                monFri: '08:00 - 17:00',
                sat: '09:00 – 12:00',
                sun: 'Closed',
                pub: '09:00 – 12:00'
            },
            latlong: '-33.901704, 18.615394',
            latlong1: '',
            mapLink: "",
            mapLinkbranch1: "",
            mapLinkbranch2: "",
            mapLinkbranch3: "",
            mapLinkbranch4: "",
            mapLinkShortLink1: "",
            mapLinkShortLink2: "",
            mapLinkShortLink3: "",
            mapLinkShortLink4: "",
            homeslider1: "",
            homeslider2: "",
            homeslider3: "",
            homeslider4: "",
            homeslider5: "",
            homeslider6: ""
        };
        return store;

    });
})();