(function () {
    var controllerId = "carRequest";
    angular.module("app").controller(controllerId, ['$scope', 'configService', carRequest]);

    function carRequest($scope,configService) {
        $scope.store = configService;
        var vm = this;
        vm.carreq = {
            name: "",
            email: "",
            phone: "",
            make: "",
            model: "",
            year: "",
            province: "",
            lessthan: "",
            colour: "",
            pdescription: ""
        };
        function spinner() {
            document.getElementById("buttsubmit").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block");
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var carRequest = "";
            var data = {
                notification: {
                    ToList: $scope.store.eMail.carRequestEmail,
                    CcList: "",
                    BccList: $scope.store.eMail.bccEmail,
                    Subject: "Vehicle Request",
                    Message: vm.carreq.pdescription
                    + "\n\nContact Number: " + vm.carreq.phone
                    + "\nProvince: " + vm.carreq.province
                    + "\nMake: " + vm.carreq.make
                    + "\nModel: " + vm.carreq.model
                    + "\nYear: " + vm.carreq.year
                    + "\nPrice: " + vm.carreq.lessthan
                    + "\nColour: " + vm.carreq.colour,
                    MessageSubject: "Vehicle Request: " + vm.carreq.make + ", " + vm.carreq.model,
                    ToName: $scope.store.eMail.carRequestEmailTo,
                    FromName: vm.carreq.name,
                    FromEmail: vm.carreq.email
                }
            };
            carRequest = JSON.stringify(data);
            $.ajax({
                url: "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {"content-type": "application/json", accept: "application/json"},
                data: carRequest,
                dataType: "json",
                success: function (response) {
                    document.getElementById("carsearch").reset();
                    angular.element("#carsearch").css("display", "none");
                    angular.element("#mailsent").css("display", "block");
                    angular.element("#spinner").css("display", "none");
                },
                error: function (response) {
                    document.getElementById("carsearch").reset();
                    document.getElementById("carsearch").style.display = "none";
                    document.getElementById("mailnotsent").style.display = "block";
                }
            });
        };
    }
})();