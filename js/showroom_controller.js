(function () {
    angular.module("app")
        .controller("showRoomall", function ($scope, $http, $state, $localStorage) {
            var vm           = this;
            vm.data          = [];
            vm.makeData      = [];
            vm.seriesData    = [];
            vm.yearData      = [];
            vm.provinceData  = [];
            vm.colourData    = [];
            vm.bodytypeData  = [];
            vm.newusedData   = [];
            vm.searchData    = [];
            vm.conditionData = [];
            vm.fourxfourData = [];
            vm.detailsdata   = {};
            vm.featuredLimit = 4;

            //$scope.$storage = $localStorage;
            localStorage['vm.data'] = JSON.stringify(vm.data);
/*            console.log(localStorage['vm.data']); */

            var getFav = JSON.parse(localStorage['vm.data']);
/*            console.log(getFav);  */

//DB settings
            var companyIdFilter       = "127";
            var dataUrlBasePath       = "https://vmgsoftware.co.za/service_api/view_stock_complete?company_id=eq." + companyIdFilter;
//Similar price filter
            var minSimPrice           = $scope.selling_price * 1 - 25000;
            var maxSimPrice           = $scope.selling_price * 1 + 25000;
            $scope.filterprice        = function (price) {
                return (price.selling_price > minSimPrice && price.selling_price < maxSimPrice)
            };
//Current URL GET
            $scope.url                = $state.href($state.current.name, $state.params, {absolute: true});
//Range Price slider
            $scope.rangeslider        = {
                min: 0,
                max: 1000000,
                options: {
                    floor: 0,
                    ceil: 1000000,
                    step: 10000,
                    translate: function (value) {
                        return 'R' + value;
                    }
                }
            };
            $scope.minFilter          = function (item) {
                return item.selling_price >= $scope.rangeslider.min;
            };
            $scope.maxFilter          = function (item) {
                return item.selling_price <= $scope.rangeslider.max;
            };
//Range Mileage slider
            $scope.rangeslidermileage = {
                min: 0,
                max: 550000,
                options: {
                    floor: 0,
                    ceil: 550000,
                    step: 20000
                }
            };
            $scope.minmileFilter      = function (item) {
                return item.mileage >= $scope.rangeslidermileage.min;
            };
            $scope.maxmileFilter      = function (item) {
                return item.mileage <= $scope.rangeslidermileage.max;
            };
// A-Z Sorting
            $scope.sort               = function () {
                $scope.options = {
                    az: 'make',
                    za: '-make',
                    pricelow: 'selling_price',
                    pricehigh: '-selling_price',
                    yearlow: 'year',
                    yearhigh: '-year'
                };

            };
// Numeric Sorting
            $scope.setItemsPerPage    = function (num) {
                $scope.itemsPerPage = num;
            };
            $scope.perPage            = {item: ["8", "16", "32", "64"]};
            $scope.showAll            = false;
            $scope.$watch("[showAll]", function () {
                $scope.itemsPerPage = !$scope.showAll;
                if ($scope.showAll === false) {
                    $scope.itemsPerPage = "8";

                }
                else {
                    $scope.itemsPerPage = vm.data.length;
                    $scope.carQuantity  = "8";
                }
            }, true);
//Keyword Search
            $scope.$watch("[searchString]", function () {
                if ($scope.searchString === '') {
                    $scope.itemsPerPage     = "8";
                    $scope.itemsPerPageHome = "8";
                }
                else {
                    $scope.itemsPerPage     = vm.data.length;
                    $scope.itemsPerPageHome = vm.data.length;
                }
            }, true);
//Drive-train Search
            $scope.$watch("[selectedDriveTrain]", function () {
                if ($scope.selectedDriveTrain === '') {
                    $scope.itemsPerPage     = "8";
                    $scope.itemsPerPageHome = "8";
                }
                else {
                    $scope.itemsPerPage     = vm.data.length;
                    $scope.itemsPerPageHome = vm.data.length;
                }
            }, true);
//Filter start------------------------------>
            $scope.currentPage = 1;
            ShowController($http, $state);
            ShowController.$inject = ["$http", $state];
            function ShowController($http, $state) {
                refreshFilters();
                resetVehicles();
                vm.detailsClick     = function (car) {
                    vm.detailsdata    = car;
                    $state.params.car = car;
                    $state.transitionTo("cardetails", {
                        car: car,
                        stock_id: car.stock_id,
                        make: car.make,
                        year: car.year,
                        mileage: car.mileage,
                        variant: car.variant,
                        selling_price: car.selling_price,
                        colour: car.colour,
                        condition: car.condition,
                        branch: car.branch,
                        extras_csv: car.extras_csv,
                        description: car.description,
                        location: car.location,
                        body_type: car.body_type,
                        province: car.province,
                        company_id: car.company_id,
                        contact_number: car.contact_number,
                        address: car.address,
                        dealer: car.dealer,
                        suburb: car.suburb,
                        url1: car.url1,
                        url2: car.url2,
                        url3: car.url3,
                        url4: car.url4,
                        url5: car.url5,
                        url6: car.url6,
                        url7: car.url7,
                        url8: car.url8,
                        url9: car.url9,
                        url10: car.url10
                    }, {});
                };
                vm.contactClick     = function (car) {
                    vm.detailsdata    = car;
                    $state.params.car = car;
                    $state.go("cardetails.enquire", {car: car});
                };
                vm.financeClick     = function (car) {
                    vm.detailsdata    = car;
                    $state.params.car = car;
                    $state.go("cardetails.financecar", {car: car});
                };
                vm.detailsClickList = function (car) {
                    vm.detailsdata              = car;
                    $state.params.car           = car;
                    $state.params.stock_id      = car.stock_id;
                    $state.params.make          = car.make;
                    $state.params.year          = car.year;
                    $state.params.variant       = car.variant;
                    $state.params.mileage       = car.mileage;
                    $state.params.colour        = car.colour;
                    $state.params.selling_price = car.selling_price;
                    $state.params.condition     = car.condition;
                    $state.params.branch        = car.branch;
                    $state.params.extras_csv    = car.extras_csv;
                    $state.params.description   = car.description;
                    $state.params.body_type     = car.body_type;
                    $state.params.location      = car.location;
                    $state.params.province      = car.province;
                    $state.params.url1          = car.url1;
                    $state.params.url2          = car.url2;
                    $state.params.url3          = car.url3;
                    $state.params.url4          = car.url4;
                    $state.params.url5          = car.url5;
                    $state.params.url6          = car.url6;
                    $state.params.url7          = car.url7;
                    $state.params.url8          = car.url8;
                    $state.params.url9          = car.url9;
                    $state.params.url10         = car.url10;
                    $state.go("cardetails", {
                        car: car,
                        stock_id: car.stock_id,
                        make: car.make,
                        year: car.year,
                        mileage: car.mileage,
                        variant: car.variant,
                        selling_price: car.selling_price,
                        colour: car.colour,
                        condition: car.condition,
                        branch: car.branch,
                        extras_csv: car.extras_csv,
                        description: car.description,
                        location: car.location,
                        body_type: car.body_type,
                        province: car.province,
                        url1: car.url1.replace(/^http:\/\//i, 'https://'),
                        url2: car.url2.replace(/^http:\/\//i, 'https://'),
                        url3: car.url3.replace(/^http:\/\//i, 'https://'),
                        url4: car.url4.replace(/^http:\/\//i, 'https://'),
                        url5: car.url5.replace(/^http:\/\//i, 'https://'),
                        url6: car.url6.replace(/^http:\/\//i, 'https://'),
                        url7: car.url7.replace(/^http:\/\//i, 'https://'),
                        url8: car.url8.replace(/^http:\/\//i, 'https://'),
                        url9: car.url9.replace(/^http:\/\//i, 'https://'),
                        url10: car.url10.replace(/^http:\/\//i, 'https://')
                    });
                };

            }

            function fetchFilteredData() {
                var currentOffset = ($scope.currentPage - 1);
                var filterUrl     = (getSelectedMakeText() !== "" ? "&make=eq." + getSelectedMakeText() : "")
                    + (getSelectedSeriesText() !== "" ? "&series=eq." + getSelectedSeriesText() : "")
                    + (getSelectedYearText() !== "" ? "&year=eq." + getSelectedYearText() : "")
                    + (getSelectedColourText() !== "" ? "&colour=eq." + getSelectedColourText() : "")
                    + (getSelectedProvinceText() !== "" ? "&province=eq." + getSelectedProvinceText() : "")
                    + (getSelectedConditionText() !== "" ? "&condition=eq." + getSelectedConditionText() : "")
                    + (getSelectedBodyTypeText() !== "" ? "&body_type=eq." + getSelectedBodyTypeText() : "");
                $http.get(dataUrlBasePath + "&offset=" + currentOffset + filterUrl).then(function (response) {
                    vm.data = response.data;
                });
                $http.get(dataUrlBasePath + "&select=year" + filterUrl).then(function (response) {
                    $scope.totalItems = response.data.length;
                });
                $scope.fourx4Total = vm.fourxfourData.length;
/*                console.log($scope.fourx4Total);  */
                event.preventDefault(); // scroll on filter
                angular.element("html,body").animate({scrollTop: "0px"}, "slow");
            }

            function getSelectedColourText() {
                return $scope.selectedColour ? $scope.selectedColour.colour : "";
            }

            function getSelectedMakeText() {
                return $scope.selectedMake ? $scope.selectedMake.make : "";
            }

            function getSelectedSeriesText() {
                return $scope.selectedSeries ? $scope.selectedSeries.series : "";
            }

            function getSelectedYearText() {
                return $scope.selectedYear ? $scope.selectedYear.year : "";
            }

            function getSelectedBodyTypeText() {
                return $scope.selectedBodyType ? $scope.selectedBodyType.body_type : "";
            }

            function getSelectedProvinceText() {
                return $scope.selectedProvince ? $scope.selectedProvince.province : "";
            }

            function getSelectedConditionText() {
                return $scope.selectedCondition ? $scope.selectedCondition.condition : "";
            }

            function resetVehicles() {
                $scope.selectedMake       = "";
                $scope.selectedSeries     = "";
                $scope.selectedYear       = "";
                $scope.selectedColour     = "";
                $scope.selectedProvince   = "";
                $scope.selectedCondition  = "";
                $scope.selectedBodyType   = "";
                $scope.searchString       = "";
                $scope.selectedDriveTrain = "";
                $scope.new_used           = "";
                $scope.showAll            = false;
                $http.get(dataUrlBasePath).then(function (response) {
                    $scope.totalItems = response.data.length;
                    vm.data = response.data;
                    for (var d = 0; d < response.data.length; d++) {
                        response.data[d].url1 = response.data[d].url1.replace(/^http:\/\//i, 'https://');
                        response.data[d].url2 = response.data[d].url2.replace(/^http:\/\//i, 'https://');
                        response.data[d].url3 = response.data[d].url3.replace(/^http:\/\//i, 'https://');
                        response.data[d].url4 = response.data[d].url4.replace(/^http:\/\//i, 'https://');
                        response.data[d].url5 = response.data[d].url5.replace(/^http:\/\//i, 'https://');
                        response.data[d].url6 = response.data[d].url6.replace(/^http:\/\//i, 'https://');
                        response.data[d].url7 = response.data[d].url7.replace(/^http:\/\//i, 'https://');
                        response.data[d].url8 = response.data[d].url8.replace(/^http:\/\//i, 'https://');
                        response.data[d].url9 = response.data[d].url9.replace(/^http:\/\//i, 'https://');
                        response.data[d].url10 = response.data[d].url10.replace(/^http:\/\//i, 'https://');
                        response.data[d].url11 = response.data[d].url11.replace(/^http:\/\//i, 'https://');
                        response.data[d].url12 = response.data[d].url12.replace(/^http:\/\//i, 'https://');
                        response.data[d].url13 = response.data[d].url13.replace(/^http:\/\//i, 'https://');
                        response.data[d].url14 = response.data[d].url14.replace(/^http:\/\//i, 'https://');
                        response.data[d].url15 = response.data[d].url15.replace(/^http:\/\//i, 'https://');
                        response.data[d].url16 = response.data[d].url16.replace(/^http:\/\//i, 'https://');
                        response.data[d].url17 = response.data[d].url17.replace(/^http:\/\//i, 'https://');
                        response.data[d].url18 = response.data[d].url18.replace(/^http:\/\//i, 'https://');
                        response.data[d].url19 = response.data[d].url19.replace(/^http:\/\//i, 'https://');
                        response.data[d].url20 = response.data[d].url20.replace(/^http:\/\//i, 'https://');

                    }
                });
            }

            function refreshFilters() {
                $http.get(dataUrlBasePath + "&select=make,series,year,province,colour,body_type,condition").then(function (response) {
                    vm.makeData      = [];
                    vm.seriesData    = [];
                    vm.yearData      = [];
                    vm.colourData    = [];
                    vm.provinceData  = [];
                    vm.conditionData = [];
                    vm.bodytypeData  = [];
                    for (var counter = 0; counter < response.data.length; counter++) {
                        vm.makeData.push(response.data[counter]);
                        vm.seriesData.push(response.data[counter]);
                        vm.yearData.push(response.data[counter]);
                        vm.colourData.push(response.data[counter]);
                        vm.provinceData.push(response.data[counter]);
                        vm.conditionData.push(response.data[counter]);
                        vm.bodytypeData.push(response.data[counter]);
                    }
                });
            }

            $scope.selectChangedMake      = function () {
                var make = getSelectedMakeText();
                if (make !== "") {
                    $http.get(dataUrlBasePath + "&select=series,year,colour,province,body_type,condition"
                        + (make !== "" ? "&make=eq." + make : "")).then(function (response) {
                        vm.seriesData    = [];
                        vm.yearData      = [];
                        vm.colourData    = [];
                        vm.provinceData  = [];
                        vm.bodytypeData  = [];
                        vm.conditionData = [];
                        for (var counter = 0; counter < response.data.length; counter++) {
                            vm.seriesData.push(response.data[counter]);
                            vm.yearData.push(response.data[counter]);
                            vm.colourData.push(response.data[counter]);
                            vm.provinceData.push(response.data[counter]);
                            vm.bodytypeData.push(response.data[counter]);
                            vm.conditionData.push(response.data[counter]);
                        }
                    });
                }
                else {
                    refreshFilters();
                }
                $scope.currentPage = 1;
                fetchFilteredData();
            };
            $scope.selectChangedSeries    = function () {
                var series = getSelectedSeriesText();
                var make   = getSelectedMakeText();
                if (series !== "") {
                    $http.get(dataUrlBasePath + "&select=year,colour,province,body_type,condition"
                        + (make !== "" ? "&make=eq." + make : "")
                        + (series !== "" ? "&series=eq." + series : "")).then(function (response) {
                        vm.yearData      = [];
                        vm.colourData    = [];
                        vm.provinceData  = [];
                        vm.bodytypeData  = [];
                        vm.conditionData = [];
                        for (var counter = 0; counter < response.data.length; counter++) {
                            vm.yearData.push(response.data[counter]);
                            vm.colourData.push(response.data[counter]);
                            vm.provinceData.push(response.data[counter]);
                            vm.bodytypeData.push(response.data[counter]);
                            vm.conditionData.push(response.data[counter]);
                        }
                    });
                }
                else {
                    refreshFilters();
                }
                $scope.currentPage = 1;
                fetchFilteredData();
            };
            $scope.selectChangedProvince  = function () {
                var series   = getSelectedSeriesText();
                var make     = getSelectedMakeText();
                var province = getSelectedProvinceText();
                if (province !== "") {
                    $http.get(dataUrlBasePath + "&select=year,colour,body_type,condition"
                        + (make !== "" ? "&make=eq." + make : "")
                        + (series !== "" ? "&series=eq." + series : "")
                        + (province !== "" ? "&province=eq." + province : "")).then(function (response) {
                        vm.yearData      = [];
                        vm.colourData    = [];
                        vm.bodytypeData  = [];
                        vm.conditionData = [];
                        for (var counter = 0; counter < response.data.length; counter++) {
                            vm.yearData.push(response.data[counter]);
                            vm.colourData.push(response.data[counter]);
                            vm.bodytypeData.push(response.data[counter]);
                            vm.conditionData.push(response.data[counter]);
                        }
                    });
                }
                else {
                    refreshFilters();
                }
                $scope.currentPage = 1;
                fetchFilteredData();
            };
            $scope.selectChangedYear      = function () {
                var year     = getSelectedYearText();
                var make     = getSelectedMakeText();
                var series   = getSelectedSeriesText();
                var province = getSelectedProvinceText();
                if (year !== "") {
                    $http.get(dataUrlBasePath + "&select=colour,body_type,condition"
                        + (make !== "" ? "&make=eq." + make : "")
                        + (series !== "" ? "&series=eq." + series : "")
                        + (province !== "" ? "&province=eq." + province : "")
                        + (year !== "" ? "&year=eq." + year : "")).then(function (response) {
                        vm.colourData    = [];
                        vm.bodytypeData  = [];
                        vm.conditionData = [];
                        for (var counter = 0; counter < response.data.length; counter++) {
                            vm.colourData.push(response.data[counter]);
                            vm.bodytypeData.push(response.data[counter]);
                            vm.conditionData.push(response.data[counter]);
                        }
                    });
                }
                else {
                    refreshFilters();
                }
                $scope.currentPage = 1;
                fetchFilteredData();
            };
            $scope.selectChangedColour    = function () {
                var make     = getSelectedMakeText();
                var series   = getSelectedSeriesText();
                var year     = getSelectedSeriesText();
                var province = getSelectedProvinceText();
                var colour   = getSelectedColourText();
                if (colour !== "") {
                    $http.get(dataUrlBasePath + "&select=body_type,condition"
                        + (make !== "" ? "&make=eq." + make : "")
                        + (series !== "" ? "&series=eq." + series : "")
                        + (province !== "" ? "&province=eq." + province : "")
                        + (year !== "" ? "&year=eq." + year : "")
                        + (colour !== "" ? "&colour=eq." + colour : "")).then(function (response) {
                        vm.bodytypeData  = [];
                        vm.conditionData = [];
                        for (var counter = 0; counter < response.data.length; counter++) {
                            vm.bodytypeData.push(response.data[counter]);
                            vm.conditionData.push(response.data[counter]);
                        }
                    });
                }
                else {
                    refreshFilters();
                }
                $scope.currentPage = 1;
                fetchFilteredData();
            };
            $scope.selectChangedBodyType  = function () {
                var make     = getSelectedMakeText();
                var series   = getSelectedSeriesText();
                var year     = getSelectedSeriesText();
                var province = getSelectedProvinceText();
                var colour   = getSelectedColourText();
                var bodytype = getSelectedBodyTypeText();
                if (bodytype !== "") {
                    $http.get(dataUrlBasePath + "&select=condition"
                        + (make !== "" ? "&make=eq." + make : "")
                        + (series !== "" ? "&series=eq." + series : "")
                        + (province !== "" ? "&province=eq." + province : "")
                        + (year !== "" ? "&year=eq." + year : "")
                        + (colour !== "" ? "&colour=eq." + colour : "")
                        + (bodytype !== "" ? "&body_type=eq." + bodytype : "")).then(function (response) {
                        vm.conditionData = [];
                        for (var counter = 0; counter < response.data.length; counter++) {
                            vm.conditionData.push(response.data[counter]);
                        }
                    });
                }
                else {
                    refreshFilters();
                }
                $scope.currentPage = 1;
                fetchFilteredData();
            };
            $scope.selectChangedCondition = function () {
                if (getSelectedConditionText() !== "") {
                }
                else {
                }
                $scope.currentPage = 1;
                fetchFilteredData();
            };

            $scope.filter      = function () {
                fetchFilteredData();
            };
            $scope.clearFilter = function () {
                refreshFilters();
                resetVehicles();
            };
            $scope.pageChanged = function () {
                fetchFilteredData();

            };
        })
    //Filter END------------------------------>

})();