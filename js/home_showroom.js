(function () {
    var controllerId = "showRoom";
    var app = angular.module("app").controller(controllerId, ["$http", "$state", "$cacheFactory", homeController]);

    function homeController($http, $state) {
        var vm = this;
        vm.data = [];
        vm.detailsdata = {};
        var companyIdGoodwood = "?company_id=eq.46";
        var companyIdKuilsriver = "&company_id=eq.30";
        var p2p = "?company_id=lt.156";
        var dataUrlBasePath = "http://wingfield.vmgdemo.co.za/webapi/view_stock_complete" + p2p;

        homeController.$inject = ["$http", "$state"];
        $http.get(dataUrlBasePath).then(function (response) {
            vm.data = response.data;
        });
        vm.detailsClick = function (car) {
            vm.detailsdata = car;
            $state.params.car = car;
            $state.go("showroom.modal", {
                car: car
            });
        };
    }


    app.controller("modalDetails", ["$http", "$state", contactController]);
    function contactController($http, $state) {
        var vm = this;
        vm.detailsdata = {};
        var urlData = "http://wingfield.vmgdemo.co.za/webapi/view_stock_complete?";
        homeController.$inject = ["$http", "$state"];
        vm.contactClick = function (car) {
            vm.detailsdata = car;
            $state.params.car = car;
            $state.go("showroom.enquire", {car: car});
        };
        vm.financeClick = function (car) {
            vm.detailsdata = car;
            $state.params.car = car;
            $state.go("showroom.financecar", {car: car});
        };
        $("[id^=th]").css("cursor", "pointer");
        $("#sl1 img").addClass("showModalImg");
        $(".slideritem").hover(function () {
            $(".modalgallery").css("opacity", "1");
        }).mouseleave(function () {
            $(".modalgallery").css("opacity", "0");
        });
        $("#th1").mouseenter(function () {
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th2").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl2 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl2 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th3").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl3 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl3 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th4").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl4 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl4 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th5").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl5 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl5 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th6").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl6 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl6 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th7").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl7 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl7 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th8").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl8 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl8 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th9").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl9 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl9 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th10").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl10 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl10 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th11").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl11 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl11 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th12").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl12 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl12 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th13").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl13 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl13 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th14").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl14 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl14 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
        $("#th15").hover(function () {
            $("#sl1 img").removeClass("showModalImg");
            $("#sl15 img").addClass("showModalImg");
        }).mouseleave(function () {
            $("#sl15 img").removeClass("showModalImg");
            $("#sl1 img").addClass("showModalImg");
        });
    }
})();