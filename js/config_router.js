(function () {
    var app = angular.module("app");

    app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $stateProvider
            .state("home", {
                url: "/",
                templateUrl: "partials/showroom.html",
                controller: "PageCtrl",
                data: {
                    meta: {
                        'title': 'Home',
                        'description': 'Welcome to Bay Motors, we offer a wide range of quality used vehicles to choose from and easy financing facilities. Come find the perfect car for you or your family!'
                    }
                }
            })
            .state("showroom", {
                templateUrl: "partials/showroom.search.list.html",
                url: "/showroom_search",
                controller: "PageCtrl",
                data: {
                    meta: {
                        'title': 'Showroom Search',
                        'description': 'We have many vehicles for you to buy'
                    }
                }
            })
            .state("disclaimer", {
                url: "/disclaimer",
                templateUrl: "partials/disclaimer.html",
                controller: "PageCtrl"
            })
            .state("complaint_resolution", {
                url: "/complaint_resolution",
                templateUrl: "partials/complaint_resolution.html",
                controller: "PageCtrl"
            })
            .state("terms", {
                url: "/terms_conditions",
                templateUrl: "partials/terms_conditions.html",
                controller: "PageCtrl"
            })
            .state("privacy", {
                url: "/privacy",
                templateUrl: "partials/privacy.html",
                controller: "PageCtrl"
            })

            .state("finance_calculator", {
                url: "/finance_calculator",
                templateUrl: "partials/finance_calculator.html",
                controller: "PageCtrl",
                data: {
                    meta: {
                        'title': 'Finance Calculator',
                        'description': 'Use our Finance Calculator to create a payment plan suited to your budget.'
                    }
                }
            })
            .state("cardetails", {
                url: "/:make/:stock_id/:year/:company_id/:dealer/:selling_price/:contact_number/:mileage/:variant/:colour/:condition/:suburb/:address/:location/:province/:body_type/:branch/:extras_csv/:description/:url1/:url2/:url3/:url4/:url5/:url6/:url7/:url8/:url9/:url10",
                params: {
                    car: null,
                    stock_id: null,
                    company_id: null,
                    make: null,
                    year: null,
                    selling_price: null,
                    mileage: null,
                    variant: null,
                    colour: null,
                    condition: null,
                    branch: null,
                    extras_csv: null,
                    description: null,
                    province: null,
                    contact_number: null,
                    url1: null,
                    url2: null,
                    url3: null,
                    url4: null,
                    url5: null,
                    url6: null,
                    url7: null,
                    url8: null,
                    url9: null,
                    url10: null
                },
                templateUrl: "partials/cardetails.html",
                controller: ["$scope", "$http", "$stateParams", function ($scope, $http, $stateParams) {
                    $scope.car            = $stateParams.car;
                    $scope.stock_id       = $stateParams.stock_id;
                    $scope.make           = $stateParams.make;
                    $scope.year           = $stateParams.year;
                    $scope.variant        = $stateParams.variant;
                    $scope.mileage        = $stateParams.mileage;
                    $scope.colour         = $stateParams.colour;
                    $scope.condition      = $stateParams.condition;
                    $scope.selling_price  = $stateParams.selling_price;
                    $scope.branch         = $stateParams.branch;
                    $scope.extras_csv     = $stateParams.extras_csv;
                    $scope.description    = $stateParams.description;
                    $scope.location       = $stateParams.location;
                    $scope.body_type      = $stateParams.body_type;
                    $scope.company_id     = $stateParams.company_id;
                    $scope.contact_number = $stateParams.contact_number;
                    $scope.address        = $stateParams.address;
                    $scope.dealer         = $stateParams.dealer;
                    $scope.suburb         = $stateParams.suburb;
                    $scope.url1           = $stateParams.url1;
                    $scope.url2           = $stateParams.url2;
                    $scope.url3           = $stateParams.url3;
                    $scope.url4           = $stateParams.url4;
                    $scope.url5           = $stateParams.url5;
                    $scope.url6           = $stateParams.url6;
                    $scope.url7           = $stateParams.url7;
                    $scope.url8           = $stateParams.url8;
                    $scope.url9           = $stateParams.url9;
                    $scope.url10          = $stateParams.url10;
                    $scope.car            = {
                        make: $scope.make,
                        stock_id: $scope.stock_id,
                        company_id: $scope.company_id,
                        dealer: $scope.dealer,
                        year: $scope.year,
                        variant: $scope.variant,
                        mileage: $scope.mileage,
                        selling_price: $scope.selling_price,
                        colour: $scope.colour,
                        condition: $scope.condition,
                        branch: $scope.branch,
                        extras_csv: $scope.extras_csv,
                        description: $scope.description,
                        location: $scope.location,
                        body_type: $scope.body_type,
                        contact_number: $scope.contact_number,
                        address: $scope.address,
                        suburb: $scope.suburb,
                        url1: $scope.url1,
                        url2: $scope.url2,
                        url3: $scope.url3,
                        url4: $scope.url4,
                        url5: $scope.url5,
                        url6: $scope.url6,
                        url7: $scope.url7,
                        url8: $scope.url8,
                        url9: $scope.url9,
                        url10: $scope.url10
                    };
                }]
            })
            .state("cardetails.enquire", {
                url: "/enquire",
                params: {car: null},
                reloadOnSearch: false,
                onEnter: ["$stateParams", "$state", "$uibModal", function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: "partials/testdrive.html",
                        resolve: {
                            car: function () {
                                return $state.params.car;
                            }
                        },
                        backdrop: "static",
                        keyboard: false,
                        reloadOnSearch: true,
                        controller: ["$scope", "$stateParams", function ($scope, $stateParams) {
                            $scope.car           = $state.params.car;
                            $scope.stock_id      = $stateParams.stock_id;
                            $scope.company_id    = $stateParams.company_id;
                            $scope.make          = $stateParams.make;
                            $scope.year          = $stateParams.year;
                            $scope.variant       = $stateParams.variant;
                            $scope.mileage       = $stateParams.mileage;
                            $scope.colour        = $stateParams.colour;
                            $scope.condition     = $stateParams.condition;
                            $scope.selling_price = $stateParams.selling_price;
                            $scope.branch        = $stateParams.branch;
                            $scope.extras_csv    = $stateParams.extras_csv;
                            $scope.description   = $stateParams.description;
                            $scope.location      = $stateParams.location;
                            $scope.url1          = $stateParams.url1;
                            $scope.url2          = $stateParams.url2;
                            $scope.url3          = $stateParams.url3;
                            $scope.url4          = $stateParams.url4;
                            $scope.url5          = $stateParams.url5;
                            $scope.url6          = $stateParams.url6;
                            $scope.url7          = $stateParams.url7;
                            $scope.url8          = $stateParams.url8;
                            $scope.url9          = $stateParams.url9;
                            $scope.url10         = $stateParams.url10;
                            $scope.car           = {
                                make: $scope.make,
                                year: $scope.year,
                                variant: $scope.variant,
                                mileage: $scope.mileage,
                                selling_price: $scope.selling_price,
                                colour: $scope.colour,
                                condition: $scope.condition,
                                branch: $scope.branch,
                                extras_csv: $scope.extras_csv,
                                description: $scope.description,
                                location: $scope.location,
                                url1: $scope.url1,
                                url2: $scope.url2,
                                url3: $scope.url3,
                                url4: $scope.url4,
                                url5: $scope.url5,
                                url6: $scope.url6,
                                url7: $scope.url7,
                                url8: $scope.url8,
                                url9: $scope.url9,
                                url10: $scope.url1
                            };
                            $scope.cancel        = function () {
                                $state.transitionTo("showroom");
                            };
                        }]
                    });
                }],
                controller: "PageCtrl"
            })
            .state("cardetails.financecar", {
                url: "/financecar",
                params: {car: null},
                reloadOnSearch: false,
                onEnter: ["$stateParams", "$state", "$uibModal", function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: "partials/modalFinance.html",
                        resolve: {
                            car: function () {
                                return $state.params.car;
                            }
                        },
                        backdrop: "static",
                        keyboard: false,
                        reloadOnSearch: true,
                        controller: ["$scope", "$stateParams", "$uibModal", function ($scope, $stateParams, $uibModal) {

                            $scope.car           = $state.params.car;
                            $scope.stock_id      = $stateParams.stock_id;
                            $scope.make          = $stateParams.make;
                            $scope.year          = $stateParams.year;
                            $scope.variant       = $stateParams.variant;
                            $scope.mileage       = $stateParams.mileage;
                            $scope.colour        = $stateParams.colour;
                            $scope.condition     = $stateParams.condition;
                            $scope.selling_price = $stateParams.selling_price;
                            $scope.branch        = $stateParams.branch;
                            $scope.extras_csv    = $stateParams.extras_csv;
                            $scope.description   = $stateParams.description;
                            $scope.url1          = $stateParams.url1;
                            $scope.url2          = $stateParams.url2;
                            $scope.url3          = $stateParams.url3;
                            $scope.url4          = $stateParams.url4;
                            $scope.url5          = $stateParams.url5;
                            $scope.url6          = $stateParams.url6;
                            $scope.url7          = $stateParams.url7;
                            $scope.url8          = $stateParams.url8;
                            $scope.url9          = $stateParams.url9;
                            $scope.url10         = $stateParams.url10;
                            $scope.car           = {
                                make: $scope.make,
                                year: $scope.year,
                                variant: $scope.variant,
                                mileage: $scope.mileage,
                                selling_price: $scope.selling_price,
                                colour: $scope.colour,
                                condition: $scope.condition,
                                branch: $scope.branch,
                                extras_csv: $scope.extras_csv,
                                description: $scope.description,
                                url1: $scope.url1,
                                url2: $scope.url2,
                                url3: $scope.url3,
                                url4: $scope.url4,
                                url5: $scope.url5,
                                url6: $scope.url6,
                                url7: $scope.url7,
                                url8: $scope.url8,
                                url9: $scope.url9,
                                url10: $scope.url1
                            };
                            $scope.cancel        = function () {
                                window.history.back();

                            };
                        }]
                    });
                }],
                controller: "PageCtrl"
            })
            .state("list", {
                templateUrl: "partials/showroom.search.list.html",
                url: "/list",
                controller: "PageCtrl",
                data: {
                    meta: {
                        'title': 'Showroom List',
                        'description': 'We have many vehicles for you to buy'
                    }
                }
            })
            .state("list.enquire", {
                url: "/:make/:stock_id/:year/:selling_price/:mileage/:variant/:colour/:condition/:location/:province/:body_type/:branch/:extras_csv/:description/:url1/:url2/:url3/:url4/:url5/:url6/:url7/:url8/:url9/:url10",
                params: {car: null},
                reloadOnSearch: false,
                onEnter: ["$stateParams", "$state", "$uibModal", function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: "partials/testdrive.html",
                        resolve: {
                            car: function () {
                                return $state.params.car;
                            }
                        },
                        backdrop: "static",
                        keyboard: false,
                        reloadOnSearch: true,
                        controller: ["$scope", "$stateParams", function ($scope, $stateParams) {
                            $scope.car           = $state.params.car;
                            $scope.stock_id      = $stateParams.stock_id;
                            $scope.make          = $stateParams.make;
                            $scope.year          = $stateParams.year;
                            $scope.variant       = $stateParams.variant;
                            $scope.mileage       = $stateParams.mileage;
                            $scope.colour        = $stateParams.colour;
                            $scope.condition     = $stateParams.condition;
                            $scope.selling_price = $stateParams.selling_price;
                            $scope.branch        = $stateParams.branch;
                            $scope.extras_csv    = $stateParams.extras_csv;
                            $scope.description   = $stateParams.description;
                            $scope.location      = $stateParams.location;
                            $scope.url1          = $stateParams.url1;
                            $scope.url2          = $stateParams.url2;
                            $scope.url3          = $stateParams.url3;
                            $scope.url4          = $stateParams.url4;
                            $scope.url5          = $stateParams.url5;
                            $scope.url6          = $stateParams.url6;
                            $scope.url7          = $stateParams.url7;
                            $scope.url8          = $stateParams.url8;
                            $scope.url9          = $stateParams.url9;
                            $scope.url10         = $stateParams.url10;
                            $scope.car           = {
                                make: $scope.make,
                                year: $scope.year,
                                variant: $scope.variant,
                                mileage: $scope.mileage,
                                selling_price: $scope.selling_price,
                                colour: $scope.colour,
                                condition: $scope.condition,
                                branch: $scope.branch,
                                extras_csv: $scope.extras_csv,
                                description: $scope.description,
                                location: $scope.location,
                                url1: $scope.url1,
                                url2: $scope.url2,
                                url3: $scope.url3,
                                url4: $scope.url4,
                                url5: $scope.url5,
                                url6: $scope.url6,
                                url7: $scope.url7,
                                url8: $scope.url8,
                                url9: $scope.url9,
                                url10: $scope.url1
                            };
                            $scope.cancel        = function () {
                                $state.transitionTo("list");
                            };
                        }]
                    });
                }],
                controller: "PageCtrl"
            })
            .state("list.financecar", {
                url: "/Finance/:make/:stock_id/:year/:selling_price/:mileage/:variant/:colour/:condition/:location/:province/:body_type/:branch/:extras_csv/:description/:url1/:url2/:url3/:url4/:url5/:url6/:url7/:url8/:url9/:url10",
                params: {
                    car: null,
                    stock_id: null,
                    make: null,
                    year: null,
                    selling_price: null,
                    mileage: null,
                    variant: null,
                    colour: null,
                    condition: null,
                    branch: null,
                    extras_csv: null,
                    description: null,
                    province: null,
                    location: null,
                    body_type: null,
                    bo: null,
                    url1: null,
                    url2: null,
                    url3: null,
                    url4: null,
                    url5: null,
                    url6: null,
                    url7: null,
                    url8: null,
                    url9: null,
                    url10: null
                },
                reloadOnSearch: false,
                onEnter: ["$stateParams", "$state", "$uibModal", function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: "partials/modalFinance.html",
                        resolve: {
                            car: function () {
                                return $state.params.car;
                            }
                        },
                        backdrop: "static",
                        keyboard: false,
                        reloadOnSearch: true,
                        controller: ["$scope", "$stateParams", "$uibModal", function ($scope, $stateParams) {
                            $scope.car           = $stateParams.car;
                            $scope.stock_id      = $stateParams.stock_id;
                            $scope.make          = $stateParams.make;
                            $scope.year          = $stateParams.year;
                            $scope.variant       = $stateParams.variant;
                            $scope.mileage       = $stateParams.mileage;
                            $scope.colour        = $stateParams.colour;
                            $scope.condition     = $stateParams.condition;
                            $scope.selling_price = $stateParams.selling_price;
                            $scope.branch        = $stateParams.branch;
                            $scope.extras_csv    = $stateParams.extras_csv;
                            $scope.description   = $stateParams.description;
                            $scope.location      = $stateParams.location;
                            $scope.body_type     = $stateParams.body_type;
                            $scope.province      = $stateParams.province;
                            $scope.url1          = $stateParams.url1;
                            $scope.url2          = $stateParams.url2;
                            $scope.url3          = $stateParams.url3;
                            $scope.url4          = $stateParams.url4;
                            $scope.url5          = $stateParams.url5;
                            $scope.url6          = $stateParams.url6;
                            $scope.url7          = $stateParams.url7;
                            $scope.url8          = $stateParams.url8;
                            $scope.url9          = $stateParams.url9;
                            $scope.url10         = $stateParams.url10;
                            $scope.car           = {
                                make: $scope.make,
                                year: $scope.year,
                                variant: $scope.variant,
                                mileage: $scope.mileage,
                                selling_price: $scope.selling_price,
                                colour: $scope.colour,
                                condition: $scope.condition,
                                branch: $scope.branch,
                                extras_csv: $scope.extras_csv,
                                description: $scope.description,
                                location: $scope.location,
                                body_type: $scope.body_type,
                                province: $scope.province,
                                url1: $scope.url1,
                                url2: $scope.url2,
                                url3: $scope.url3,
                                url4: $scope.url4,
                                url5: $scope.url5,
                                url6: $scope.url6,
                                url7: $scope.url7,
                                url8: $scope.url8,
                                url9: $scope.url9,
                                url10: $scope.url10
                            };
                            $scope.cancel        = function () {
                                $state.transitionTo("list");

                            };
                        }]
                    });
                }],
                controller: "PageCtrl"
            })

            .state("car_service", {
                url: "/car_service",
                templateUrl: "partials/car_service.html",
                controller: "PageCtrl",
            })
            .state("car_request", {
                url: "/car_request",
                templateUrl: "partials/car_request.html",
                controller: "PageCtrl",
            })
            .state("sell_car", {
                url: "/sell_car",
                templateUrl: "partials/sell_car.html",
                controller: "PageCtrl",
                data: {
                    meta: {
                        'title': 'Trade In or Sell',
                        'description': 'Trade in your car by filling out our easy to use form.'
                    }
                }
            })
            .state("finance", {
                url: "/finance",
                templateUrl: "partials/finance.html",
                controller: "PageCtrl",
                data: {
                    meta: {
                        'title': 'Finance Application',
                        'description': 'Use our EazyFin Finance Application to apply for finance.'
                    }
                }
            })
            .state("contact", {
                url: "/contact",
                templateUrl: "partials/contact.html",
                controller: "PageCtrl",
                data: {
                    meta: {
                        'title': 'Contact Us',
                        'description': 'Contact us using our contact form. We would love to help you find your dream car.'
                    }
                }
            })
            .state("testimonials", {
                url: "/testimonials",
                templateUrl: "partials/testimonials.html",
                controller: "PageCtrl",
            })
            .state("meetdealer", {
                url: "/meetdealer",
                templateUrl: "partials/meetdealers.html",
                controller: "PageCtrl",
                data: {
                    meta: {
                        'title': 'Home page',
                        'description': 'This is the description shown in Google search results'
                    }
                }
            })
            .state("social", {
                url: "/social",
                templateUrl: "partials/social.html",
                controller: "PageCtrl",
            })
            .state("testimonial", {
                url: "/testimonial",
                templateUrl: "partials/testimonial.html",
                controller: "PageCtrl",
            })
            .state("404", {
                url: "/404",
                templateUrl: "partials/404.html",
                controller: "PageCtrl",
                data: {
                    meta: {
                        'title': 'Page Not Found',
                        'description': 'Sorry, we couldn\'t find the page you are looking for'
                    }
                }
            })
            .state("otherwise", {url: "/404", templateUrl: "partials/404.html", controller: "PageCtrl"});
        $urlRouterProvider.when("", "/home");
        $urlRouterProvider.otherwise("/404");

    });
    app.controller("PageCtrl", function ($scope) {

    });
    app.controller("dealer", function ($scope) {
        $scope.getSlugifiedName = function (item) {
            return $filter('slugify')(item.name);
        }
    });

    app.run(["$rootScope", "$uibModalStack", function ($rootScope, $uibModalStack) {
        $rootScope.$on("$stateChangeStart", function () {
            var top = $uibModalStack.getTop();
            if (top) {
                $uibModalStack.dismiss(top.key);
            }
        });
    }]);

    app.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        $stateProvider.state("/", {url: "/home", templateUrl: "partials/showroom.html"});
    }]);

})();