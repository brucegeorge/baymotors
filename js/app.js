(function () {
    var controllerID       = "mainCTRL";
    var sliderControllerID = "slider";
    angular.module("app", [
        "ngRoute",
        "ngResource",
        "ngMap",
        "ui.bootstrap",
        "angularValidator",
        "ngSanitize",
        "ui.router",
        "ngStorage",
        "ngParallax",
        "rzModule",
        "flow",
        "ngSocial",
        "sticky",
        "mailchimp",
        "angular.backtop",
        "angular.filter",
        "720kb.socialshare",
        "updateMeta",
        "angularUtils.directives.dirPagination"

    ])
        .directive('scrollOnClick', function () {
            return {
                restrict: 'A',
                link: function (scope, $elm, attrs) {
                    var idToScroll = attrs.href;
                    $elm.on('click', function () {
                        var $target;
                        if (idToScroll) {
                            $target = $(idToScroll);
                        }
                        else {
                            $target = $elm;
                        }
                        $("body").animate({scrollTop: $target.offset().top}, "slow");
                    });
                }
            }
        })
        .directive('flowImageResize', function($q) {
            return {
                'require': 'flowInit',
                'link': function(scope, element, attrs) {
                    scope.$flow.opts.preprocess = function (chunk) {
                        chunk.fileObj.promise.then(function () {
                            if (!chunk.fileObj.resized) {
                                chunk.fileObj.resized = true;
                                chunk.fileObj.retry();
                            }
                        });
                        if (chunk.fileObj.resized) {
                            chunk.preprocessFinished();
                        }
                    };
                    scope.$flow.on('filesSubmitted', function (files) {
                        angular.forEach(files, function (file) {
                            var nativeFile = file.file;// instance of File, same as here: https://github.com/flowjs/ng-flow/blob/master/src/directives/img.js#L13
                            file.file = null;// do not display it
                            var deferred = $q.defer();
                            file.promise = deferred.promise;

                            loadImage(
                                nativeFile,
                                function (canvas) {
                                    canvas.toBlob(function (blob) {
                                        file.file = blob;
                                        file.size = blob.size;
                                        deferred.resolve();
                                        scope.$digest();
                                    });
                                },
                                {
                                    canvas: true,
                                    crop: true,
                                    maxWidth: 500,
                                    maxHeight: 500
                                }
                            );
                        });
                    })
                }
            };
        })
        .directive('myFlowImg', [function() {
            return {
                'scope': false,
                'require': '^flowInit',
                'link': function(scope, element, attrs) {
                    var file = attrs.myFlowImg;
                    scope.$watch(file + '.file', function (file) {
                        if (!file) {
                            return ;
                        }
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function (event) {
                            scope.$apply(function () {
                                attrs.$set('src', event.target.result);
                            });
                        };
                    });
                }
            };
        }])
        .controller("mainCTRL", function ($scope) {
            $scope.filterShow = false;

        })
        .controller("sliderController", function ($scope, $location, $anchorScroll) {
            $scope.scrollTo = function (id) {
                $location.hash(id);
                $anchorScroll();
            }
        })
        .controller('footer', function ($scope) {
            $scope.footerPara = '/img/paraback.png';

        })
        .controller(controllerID, ['$scope', 'configService', function ($scope, configService) {
            $scope.store = configService;

        }])
        .controller('topSocial', ['$scope', 'configService', function ($scope, configService) {
            $scope.store = configService;

        }])
        .controller('tradeIn', ['$scope', 'configService', function ($scope, configService) {
            $scope.store = configService;

        }])
        .controller('testDrive', ['$scope', 'configService', function ($scope, configService) {
            $scope.store = configService;

        }])
        .controller('footerConfig', ['$scope', 'configService', function ($scope, configService) {
            $scope.store = configService;

        }])
        .controller('modalFinance', ['$scope', 'configService', function ($scope, configService) {
            $scope.store = configService;

        }])
        .controller('testimonial', ['$scope', 'configService', function ($scope, configService) {
            $scope.store = configService;

        }])
        .controller(sliderControllerID, ['$scope', 'configService', function ($scope, configService) {
            $scope.store = configService;

        }])
        .controller("MapCNTRL", ['$scope', 'NgMap', 'configService', function ($scope, NgMap, configService) {
            $scope.store = configService;
            NgMap.getMap().then(function (map) {
            });

        }])
        .filter("unique", function () {
            return function (collection, keyname) {
                var output = [], keys = [];
                angular.forEach(collection, function (item) {
                    var key = item[keyname];
                    if (keys.indexOf(key) === -1) {
                        keys.push(key);
                        output.push(item);
                    }
                });
                return output;
            };
        });
    $(document).on("click", ".navbar-collapse.in", function (e) {
        if ($(e.target).is("a")) {
            $(this).collapse("hide");
        }
    });
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop < 200) {
            maxWidth = 180;
            boxShad  = '0px 11px 16px -14px #000';

        }
        else if (scrollTop > 400) {
            maxWidth = 110;
            boxShad  = '0px 0px 0px 0px #000';

        }
        else {
            maxWidth = 180 - 70 * ((scrollTop - 200)) / 200;
        }



    });



})();