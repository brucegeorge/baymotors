// JavaScript Document
$( document ).ready(function() {
'use strict';
(function(){
$.fn.extend({
    animateCss: function (fadeInDown) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + fadeInDown).one(animationEnd, function() {
            $(this).removeClass('animated ' + fadeInDown);
        });
    }
});
$('#pgc-28-4-0').animateCss('fadeInDown');
$('#pgc-28-4-1').animateCss('fadeInDown');
$('#pgc-28-4-2').animateCss('fadeInDown');
$('#pgc-28-4-3').animateCss('fadeInDown');
$('#pgc-28-4-4').animateCss('fadeInDown');
$('#pgc-28-4-5').animateCss('fadeInDown');
})();



});